# TAMUSINT
TAMUSINT is an open source intelligence (OSINT) tool that allows you to quickly and easily search the [Texas A&M student directory](https://services.tamu.edu/directory-search/).  It displays a list of students matching the provided search criteria.  For each student, it shows the information provided by the directory (full name, classification, major, and phone number) and can also fetch the corresponding email address and URL from each student's profile.  Multiple search criteria can be used if separated by commas.

This tool can also be used for batch searches.  Given an input file containing a list of names, TAMUSINT will list the corresponding directory entries.

It can also export the resulting list into a file in YAML format.


The command line arguments for TAMUSINT are as follows:

| Argument  | Function                                                    |
| --------- | ----------------------------------------------------------- |
| -d        | Display Name                                                |
| -f        | First Name                                                  |
| -l        | Last Name                                                   |
| -c        | Class (U1, U2, etc.)                                        |
| -m        | Major                                                       |
| -i        | Input File (Plain text file containing one name per line)   |
| -o        | Output File (File will be created or overwritten as needed) |
| -p        | Get profiles (email/URL) for each person                    |
| -v        | Verbose mode                                                |
| -s        | Single threaded mode                                        |

The "any" keyword can be used as a wildcard for search criteria.

Example usage:
```
tamusint -d "John Doe" [-c U4] [-m Business] [-o output.yml]
tamusint -f John -l Doe [-c U4] [-m Business] [-o output.yml]
tamusint -i input.txt [-o output.yml]
```
